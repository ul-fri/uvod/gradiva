# Gradiva za uvod v študij računalništva

Uvod v študij računalništva je kratek tečaj programiranja in ponovitev 
srednješolske matematike, ki poteka konec septembra na 
[Fakulteti za računalništvo in informatiko, Univerze v Ljubljani](https://www.fri.uni-lj.si).

